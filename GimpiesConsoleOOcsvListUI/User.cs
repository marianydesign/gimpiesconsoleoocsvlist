namespace GimpiesConsoleOOcsvListUI
{
    public class User
    {
        // Constructor
        public User(int userid, string username, string email, string password, string userrole)
        {
            _UserId = userid;
            _UserName = username;
            _Email = email;
            _Password = password;
            _UserRole = userrole;
        }
        private int _UserId;
        private string _UserName;
        private string _Email;
        private string _Password;
        private string _UserRole;

        public int userid
        {
            get { return _UserId; }
            set { _UserId = value; } 
        }
        public string username
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        public string email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        public string password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        public string userrole
        {
            get { return _UserRole; }
            set { _UserRole = value; }
        }
        
    }
}