using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Globalization;
using System.Linq;
using CsvHelper;

namespace GimpiesConsoleOOcsvListUI
{
    public class Stock
    {
        // Stock methods to handle the stock
        public void StockManager()
        {
            
            // First line to skip a row and position all users correctly under the right headers
            Gimpies gimpies0 = new Gimpies("", 0.0, "", 0.0, 0);
            // Create user default instances
            Gimpies gimpies1 = new Gimpies("Nike", 44.0, "Blue", 100.0, 10);
            Gimpies gimpies2 = new Gimpies("Adidas", 43.5, "White", 89.0, 20);
            Gimpies gimpies3 = new Gimpies("Puma", 42.0, "Red", 119.0, 15);

            // List of users (with a list you can add, get and remove items in the list)
            List<Gimpies> gimpies = new List<Gimpies>();
            gimpies.Add(gimpies0);
            gimpies.Add(gimpies1);
            gimpies.Add(gimpies2);
            gimpies.Add(gimpies3);
        }                
    }
}