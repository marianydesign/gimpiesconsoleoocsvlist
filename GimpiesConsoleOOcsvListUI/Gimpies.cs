namespace GimpiesConsoleOOcsvListUI
{
    public class Gimpies
    {
        // Constructor
        public Gimpies(string model, double size, string color, double price, int stock)
        {
            _Model = model;
            _Size = size;
            _Color = color;
            _Price = price;
            _Stock = stock;
        }

        private string _Model;
        private double _Size;
        private string _Color;
        private double _Price;
        private int _Stock;

        public string Model
        {
            get { return _Model; }
            set { _Model = value; }
        }

        public double Size
        {
            get { return _Size; }
            set { _Size = value; }
        }

        public string Color
        {
            get { return _Color; }
            set { _Color = value; }
        }

        public double Price
        {
            get { return _Price; }
            set { _Price = value; }
        }

        public int Stock
        {
            get { return _Stock; }
            set { _Stock = value; }
        }
    }
}