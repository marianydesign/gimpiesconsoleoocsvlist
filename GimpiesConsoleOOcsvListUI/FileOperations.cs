using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Globalization;
using System.Linq;
using CsvHelper;

namespace GimpiesConsoleOOcsvListUI
{
    // Handles CRUD within CSV files and able to save them
    public class FileOperations
    {
        // Appends to CSV file from List
        public void WriteUsersToCSV(List<User> users)
        {
            
            // overwrite the file each time; indicated by the `false` parameter
            using (var writer = new StreamWriter("users.csv", false))
            using (var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                // Order the list again so the userid's are asc
                users = users.OrderBy(u => u.userid).ToList();
                
                // csvWriter.Configuration.HasHeaderRecord = false; // commented out as we write the whole file every time including the header
                csvWriter.WriteRecords(users);
                Console.WriteLine("New user added to users.csv");
            }
        }

        // Writes to CSV file from List
        public void WriteUsersToNewCSVFile(List<User> users)
        {
            using (var writer = new StreamWriter("users.csv"))
            using (var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                // csvWriter.Configuration.Delimiter = ";";
                csvWriter.Configuration.HasHeaderRecord = true;
                csvWriter.Configuration.AutoMap<User>();
                csvWriter.WriteHeader<User>();
                csvWriter.WriteRecords(users);
                
                Console.WriteLine("New file users.csv created!");
            }
        }

        // Reads from CSV file and displays content from it 
        public void ReadUsersFromCSV()
        {
            using (var reader = new StreamReader("users.csv"))
            using (var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                try
                {
                    // csvReader.Configuration.Delimiter = ";";
                    // csvReader.Configuration.IgnoreBlankLines = true;
                    csvReader.Configuration.HasHeaderRecord = true;
                    // csvReader.Configuration.PrepareHeaderForMatch = (string header, int index) => header.ToLower();
                    // csvReader.Configuration.MissingFieldFound = null;
                    csvReader.Read();
                    csvReader.ReadHeader();
                    
                    // Store all content inside a new List as objetcs
                    var users = csvReader.GetRecords<User>().ToList();
                    
                    // Loop through the List and show them in Console (connect to values in the constructor User.cs record.username etc..)
                    foreach (var user in users)
                    {
                        Console.WriteLine($"{user.userid} | {user.username } | {user.email} | {user.password} | {user.userrole}");
                    }
                }
                catch (CsvHelper.HeaderValidationException exception)
                {
                    Console.WriteLine(exception);
                }
            }
        }

        // Writes to CSV file from List
        public void SaveGimpiesToCSV(List<Gimpies> gimpies)
        {
            // using (var mem = new MemoryStream())
            // using (var writer = new StreamWriter(mem))
            using (var writer = new StreamWriter("gimpies.csv"))
            using (var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csvWriter.Configuration.Delimiter = ";";
                csvWriter.Configuration.HasHeaderRecord = true;
                csvWriter.Configuration.AutoMap<Gimpies>();
                csvWriter.WriteHeader<Gimpies>();
                csvWriter.WriteRecords(gimpies);
                writer.Flush();
                // var result = Encoding.UTF8.GetString(mem.ToArray());
                // Console.WriteLine(result);
            }
        }
            
        
    }
    
}