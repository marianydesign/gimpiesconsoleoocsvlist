using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Globalization;
using System.Linq;
using CsvHelper;

namespace GimpiesConsoleOOcsvListUI
{
    public class UserManager
    {
        public void Register(List<User> users)
        {
            // Sort List to get last userid and make that the one to to add up userid for new user
            User usr = users.OrderByDescending(u => u.userid).FirstOrDefault();
            
            // Returning userid which is one higher than last one
            int userid = (usr == null ? 1 : usr.userid + 1);
            
            // Get user input
            Console.WriteLine("Enter username:");
            string username = Console.ReadLine();

            Console.WriteLine("Enter email:");
            string email = Console.ReadLine();

            Console.WriteLine("Enter password:");
            string password = Console.ReadLine();

            Console.WriteLine("Enter userrole (typ admin, purchase or sales):");
            string userrole = Console.ReadLine();

            // Create fresh instance to save input in memory
            User user = new User(userid, username, email, password, userrole);
    
            // Adds the user to the excisting list           
            users.Add(user);

            FileOperations fo = new FileOperations();
            // Calling the method from FileOperations.cs to write the List here to a CSV file
            fo.WriteUsersToCSV(users);
            
        }

        public void Remove(List<User> users)
        {
        
            // Show currect list of users from users.csv
            FileOperations fo = new FileOperations();
            // Calling the method from FileOperations.cs to read the CSV file users.csv
            fo.ReadUsersFromCSV();
            Console.WriteLine("");

            // Get user input
            Console.WriteLine("Enter the correct user ID to remove:");
            string username = Console.ReadLine();
    
            // Adds the user to the excisting list           
            // users.Remove("Beheer");

            // FileOperations fo = new FileOperations();
            // Calling the method from FileOperations.cs to write the List here to a CSV file
            // fo.WriteUsersToCSV(users);
            
        }
    }
}