﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Globalization;
using System.Linq;
using CsvHelper;

namespace GimpiesConsoleOOcsvListUI
{
    class Program
    {
        static void Main(string[] args)
        {         
            // Read csv file first and read list from method in UserList.cs
            List<User> users = UserList.LoadUsersFromCSV();    

            // Welcome message
            Console.WriteLine("Welcome to the Gimpies Console Application! Choose 1 to login or 0 to exit this application:");

            // Create login instance (for future use)
            LoginManager loginMgr = new LoginManager();

            // Create stock instance
            Stock stock = new Stock();

            // Get input from user
            string input = Console.ReadLine();

            // Set to false to check if login fails or not
            bool successfull = false;

            while (!successfull)
            {
                if(input == "1")
                {
                    Console.WriteLine("Enter your username:");
                    string username = Console.ReadLine();
                    Console.WriteLine("Enter your password:");
                    string password = Console.ReadLine();

                    foreach (User user in users)
                    {
                        if (username == user.username && password == user.password && user.userrole == "admin")
                            {
                                // Create Admin instance to be able to call methods in that class
                                Admin am = new Admin();
                                // Calling the method in Admin.cs to start Menu logic
                                am.AdminLoggedIn(users); 
                                
                                successfull = true;
                                break; 
                            }

                        if (username == user.username && password == user.password && user.userrole == "purchase")
                            {
                                // Create Purchase instance to be able to call methods in that class
                                Purchase pc = new Purchase();
                                // Calling the method in Purchase.cs to start Menu logic
                                pc.PurchaseLoggedIn(users);
                                
                                successfull = true;
                                break; 
                            }
                        
                        if (username == user.username && password == user.password && user.userrole == "sales")
                            {
                                // Create Sales instance to be able to call methods in that class
                                Sales sl = new Sales();
                                // Calling the method in Sales.cs to start Menu logic
                                sl.SalesLoggedIn(users);
                                
                                successfull = true;
                                break; 
                            }
                    }

                        if (!successfull)
                            {
                                Console.WriteLine("Your username or password is incorrect, try again !!!");
                            }
                }

                else if (input == "0")
                {          
                    Console.WriteLine("Good bye!");
                    Environment.Exit(-1);
                }

                else
                {
                    Console.WriteLine("Try again !!!");
                    break;
                }
            }
            
            // // Loop over stored users within instances inside the list where users are added
            // foreach (User user in users)
            // {
            
            //     if(loginMgr.Login(user))
            //     {
            //         // Login successfull
            //         Console.WriteLine("Login user " + user.UserName);

            //     }
            //     else
            //     {
            //         // Not successfull

            //     }
            // }  
            
        }
    }
}
