using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Globalization;
using System.Linq;
using CsvHelper;

namespace GimpiesConsoleOOcsvListUI
{
    public class UserList
    {
        // public static List<User> DefaultUsers()
        // {
        //     // First line to skip a row and position all users correctly under the right headers
        //     User user0 = new User("", "", "", "");
        //     // Create user default instances
        //     User user1 = new User("Beheer", "beheer@gimpies.nl", "123", "admin");
        //     User user2 = new User("Inkoop", "inkoop@gimpies.nl", "123", "purchase");
        //     User user3 = new User("Verkoop", "verkoop@gimpies.nl", "123", "sales");
        //     // List of users (with a list you can add, get and remove items in the list)
        //     List<User> users = new List<User>();
        //     users.Add(user0);
        //     users.Add(user1);
        //     users.Add(user2);
        //     users.Add(user3);

        //     // Return all the users in the List to other classes
        //     return users;
        // }

        public static List<User> LoadUsersFromCSV()
        {
            using (var reader = new StreamReader("users.csv"))
            using (var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                // csvReader.Configuration.Delimiter = ";";
                // csvReader.Configuration.IgnoreBlankLines = true;
                csvReader.Configuration.HasHeaderRecord = true;
                // csvReader.Configuration.PrepareHeaderForMatch = (string header, int index) => header.ToLower();
                // csvReader.Configuration.MissingFieldFound = null;
                csvReader.Read();
                csvReader.ReadHeader();
                    
                // Store all content inside a new List as objetcs
                var users = csvReader.GetRecords<User>().ToList();  
                    
                return users;
                
                // try
                // {
                //                   
                // }
                // catch (CsvHelper.HeaderValidationException exception)
                // {
                //     Console.WriteLine(exception);
                // }
                
            }
            
        }
    }
}