using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Globalization;
using System.Linq;
using CsvHelper;
using System.Threading;
using System.Threading.Tasks;

namespace GimpiesConsoleOOcsvListUI
{
    public class Admin
    {
        // Menu logic for default user Admin
        public void AdminLoggedIn(List<User> users)
        {
            Console.WriteLine();
            Console.WriteLine("You have successfully logged in as Admin !!!");
            Console.WriteLine();

            // Show AdminMenu
            bool showMenu = true;
            while (showMenu)
            {
                showMenu = AdminMenu(users);
            }
        }

        public static bool AdminMenu(List<User> users)
        {
            // await Task.Delay(2000);
            Console.WriteLine();
            Console.WriteLine("Choose from the following options:");
            Console.WriteLine();
            Console.WriteLine("1. Show current stored users.");
            Console.WriteLine("2. Add users.");
            Console.WriteLine("3. Remove users.");
            Console.WriteLine("4. Update users.");
            Console.WriteLine("0. Exit.");
            Console.WriteLine();
            Console.Write("\r\nSelect an option: ");
            
            switch (Console.ReadLine())
            {
                case "1":
                    // Show currect list of users from users.csv
                    FileOperations fo = new FileOperations();
                    // Calling the method from FileOperations.cs to read the CSV file users.csv
                    fo.ReadUsersFromCSV();
                    return true;
                case "2":
                    UserManager au = new UserManager();
                    au.Register(users);  
                    return true;
                case "3":
                    UserManager ru = new UserManager();
                    ru.Remove(users);
                    return true;
                case "4":
                    return true;
                case "0":
                    Console.WriteLine("Good bye!");
                    return false;
                default:
                    return true;
            }
        }
    }
}